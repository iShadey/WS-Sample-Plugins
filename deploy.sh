#!/bin/bash
ARTIFACT=`mvn -q -Dexec.executable="echo" -Dexec.args='${project.artifactId}' --non-recursive org.codehaus.mojo:exec-maven-plugin:1.3.1:exec 2>/dev/null`
VERSION=`mvn -q -Dexec.executable="echo" -Dexec.args='${project.version}' --non-recursive org.codehaus.mojo:exec-maven-plugin:1.3.1:exec 2>/dev/null`
echo "Deploying ${ARTIFACT} ${VERSION}"
if [ -f target/${ARTIFACT}-${VERSION}-jar-with-dependencies.jar ]
then
    mv target/${ARTIFACT}-${VERSION}-jar-with-dependencies.jar ${ARTIFACT}-${VERSION}.jar
else
    mv target/${ARTIFACT}-${VERSION}.jar ${ARTIFACT}-${VERSION}.jar
fi