package com.degoos.spawntp.command;


import com.degoos.spawntp.SpawnTP;
import com.degoos.wetsponge.command.WSCommand;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.text.WSText;

import java.util.ArrayList;
import java.util.List;

public class RemoveSpawnCommand extends WSCommand {

	public RemoveSpawnCommand () {
		super("removeSpawn", "Removes the latest set spawn location.");
	}


	@Override
	public void executeCommand (WSCommandSource wsCommandSource, String s, String[] strings) {
		if (!wsCommandSource.hasPermission("spawnTp.admin"))
			wsCommandSource.sendMessage(WSText.builder("You don't have permission to use this command.").color(EnumTextColor.RED).build());
		else if (SpawnTP.getSpawnLocations().size() > 0 && strings.length == 1) {
			int index = SpawnTP.getSpawnLocations().size() - 1;
			SpawnTP.getSpawnLocations().remove(index);
			wsCommandSource.sendMessage(WSText.builder("The spawn location " + index + " has been removed!").color(EnumTextColor.GREEN).build());
		}
		else {
			try {
				int index = Integer.valueOf(strings[0]);
				if (SpawnTP.getSpawnLocations().size() < index) {
					SpawnTP.getSpawnLocations().remove(index);
					wsCommandSource.sendMessage(WSText.builder("The spawn location " + index + " has been removed!").color(EnumTextColor.GREEN).build());
				}
				else wsCommandSource.sendMessage(WSText.builder("There is no spawn #" + index + ".").color(EnumTextColor.RED).build());
			} catch (Exception e) {
				wsCommandSource.sendMessage(WSText.builder("You can only remove spawn points by its index number.").color(EnumTextColor.RED).build());
			}
		}
	}


	@Override
	public List<String> sendTab (WSCommandSource wsCommandSource, String s, String[] strings) {
		return new ArrayList<>();
	}
}
