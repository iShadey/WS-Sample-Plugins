package com.degoos.spawntp;


import com.degoos.spawntp.command.AddSpawnCommand;
import com.degoos.spawntp.command.ReloadCommand;
import com.degoos.spawntp.command.RemoveSpawnCommand;
import com.degoos.spawntp.command.SpawnCommand;
import com.degoos.spawntp.listener.PlayerListener;
import com.degoos.spawntp.loader.ManagerLoader;
import com.degoos.spawntp.manager.FileManager;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.command.WSCommandManager;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.plugin.WSPlugin;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.world.WSLocation;

import java.util.ArrayList;
import java.util.List;

public class SpawnTP extends WSPlugin {
    private static SpawnTP instance;
    private static List<WSLocation> spawnLocations;


    @Override
    public void onEnable() {

        WetSponge.getServer().getConsole().sendMessage(WSText.builder("Loading SpawnTP version ").color(EnumTextColor.YELLOW).append(WSText.builder
                (getPluginDescription().getVersion()).color(EnumTextColor.RED).append(WSText.builder("!").color(EnumTextColor.YELLOW).build()).build()).build());

        instance = this;
        spawnLocations = new ArrayList<>();
        ManagerLoader.load();

        WSCommandManager cm = WetSponge.getCommandManager();
        cm.addCommand(new AddSpawnCommand());
        cm.addCommand(new RemoveSpawnCommand());
        cm.addCommand(new SpawnCommand());
        cm.addCommand(new ReloadCommand());

        WetSponge.getEventManager().registerListener(new PlayerListener(), this);

        WetSponge.getServer().getConsole().sendMessage(WSText.builder("Done!").color(EnumTextColor.YELLOW).build());
    }


    public static SpawnTP getInstance() {
        return instance;
    }


    public static List<WSLocation> getSpawnLocations() {
        return spawnLocations;
    }


    public static void addSpawnLocation(WSLocation newLocation, boolean saveFile) {
        spawnLocations.add(newLocation);
        if (saveFile) ManagerLoader.getManager(FileManager.class).saveSpawnsList(SpawnTP.getSpawnLocations());
    }
}
